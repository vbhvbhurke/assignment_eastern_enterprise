<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

class ReportController extends Controller
{

    public function getRow(Carbon $date)
    {
        return $date->format('F').",".$this->getSalaryDate($date).",".$this->getBonusDate($date)."\n";
    }

    private function getsalaryDate(Carbon $date)
    {
        if ($date->endOfMonth()->isWeekday()) {
            return $date->endOfMonth()->format('d-M-Y');
        }

        return  Carbon::createFromTimeStamp(strtotime("last Friday", $date->timestamp))->format('d-M-Y');
    }

    private function getBonusDate(Carbon $date)
    {
        $bonusPayoutDate = Carbon::createFromDate($date->year, $date->month,15)->addMonthsNoOverflow(1);
        if ($bonusPayoutDate->isWeekday()) {
            return $bonusPayoutDate->format('d-M-Y');
        }

        return  Carbon::createFromTimeStamp(strtotime("next wednesday", $bonusPayoutDate->timestamp))->format('d-M-Y');
    }
}
