<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\ReportController;
use Carbon\Carbon;

class GeneratePayoutDates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:payout-dates {filename=payoutreminder}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generaet Payout Reminder CSV';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ReportController $report)
    {
        parent::__construct();
        $this->report = $report;
        $this->content = '';
        $this->headers = "Month,Salary Payout Date,Bonus Payout Date";
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->addHeaders();
        $this->getCSVContent();
        file_put_contents($this->argument('filename').'csv',$this->content);
        $this->info('CSV file has been generated in the project root directory');
    }

    public function addHeaders()
    {
        $this->content.="Month,Salary Payout Date,Bonus Payout Date\n";
    }

    public function getCSVContent()
    {
        $year = Carbon::now()->year;

        for($i=1;$i<=12;$i++)
        {
            $date = Carbon::create($year,$i,1);
            $this->content .= $this->report->getRow($date);
        }
    }

}
